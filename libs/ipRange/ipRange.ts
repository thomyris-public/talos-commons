import { Address4 } from 'ip-address';

import { ERROR } from '../enums/error';
import { Address } from '../address/address';

export class IpRange {
  /** @type {Address} The range's start as an Address */
  public start: Address;

  /** @type {Address} The range's end as an Address */
  public end: Address;

  constructor(range: { start: string; end: string }) {
    this.start = new Address(range.start);
    this.end = new Address(range.end);
  }

  /**
   * Splits a range by an array of ranges
   * Returns an array of ranges
   */
  cutRange(excludedRanges: IpRange[]): IpRange[] {
    // check if some excluded ranges are overlaping between each others
    for (let i = 0; i < excludedRanges.length; i++) {
      const excludedRange: IpRange = excludedRanges[i];

      const isOverlap: boolean = excludedRanges.some((elmt: IpRange, index: number) => {
        if (index === i) return false;
        return IpRange.isOverlaping(elmt, excludedRange);
      });

      if (isOverlap === true) throw new Error(ERROR.RANGE_OVERLAP);
    }

    // cut the base range by the excluded ranges
    const splitedRange: IpRange[] = [this];

    for (let i = 0; i < excludedRanges.length; i++) {
      const excludedRange: IpRange = excludedRanges[i];
      const tmpRanges: IpRange[] = [...splitedRange];

      IpRange.checkRange(this, excludedRange);

      for (let j = 0; j < tmpRanges.length; j++) {
        const savedRange: IpRange = tmpRanges[j];

        const isInRange: boolean = IpRange.isInRange(savedRange, excludedRange);
        if (isInRange === false) continue;

        const newEnd = Address4.fromBigInteger(excludedRange.start.bigInteger.subtract(new Address('0.0.0.1').bigInteger));
        const newStart = Address4.fromBigInteger(excludedRange.end.bigInteger.add(new Address('0.0.0.1').bigInteger));

        const subRangeStart: IpRange = new IpRange({
          start: savedRange.start.address,
          end: newEnd.address
        });

        const subRangeEnd: IpRange = new IpRange({
          start: newStart.address,
          end: savedRange.end.address
        });

        splitedRange.splice(j, 1);

        if (excludedRange.end.bigInteger.equals(savedRange.end.bigInteger) === false) splitedRange.push(subRangeEnd);
        if (excludedRange.start.bigInteger.equals(savedRange.start.bigInteger) === false) splitedRange.push(subRangeStart);
      }
    }

    splitedRange.sort((first: IpRange, second: IpRange) => {
      if (first.start.bigInteger > second.start.bigInteger) return 1;
      return -1;
    });

    return splitedRange;
  }

  /**
   * Check if the second range is OUTSIDE of the firstRange or is OVERSIZED
   */
  static checkRange(firstRange: IpRange, secondRange: IpRange): boolean {
    const firstSize: number = firstRange.end.compareTo(firstRange.start);
    const secondSize: number = secondRange.end.compareTo(secondRange.start);

    if (firstSize < secondSize) throw new Error(ERROR.RANGE_OVERSIZED);

    const isInRange: boolean = this.isInRange(firstRange, secondRange);
    if (isInRange === false) throw new Error(ERROR.RANGE_OUTSIDE);
    return true;
  }

  /**
   * Check if the secondRange is inside the firstRange
   */
  static isInRange(firstRange: IpRange, secondRange: IpRange): boolean {
    const isAboveStart: boolean = secondRange.start.compareTo(firstRange.start) >= 0;
    const isBelowEnd: boolean = secondRange.end.compareTo(firstRange.end) <= 0;

    if (isAboveStart === false || isBelowEnd === false) return false;
    return true;
  }

  /**
   * Return true if the secondRange is overlaping the firstRange
   */
  static isOverlaping(firstRange: IpRange, secondRange: IpRange): boolean {
    const startIsBelowStart: boolean = firstRange.start.compareTo(secondRange.start) <= 0;
    const startIsBelowEnd: boolean = firstRange.start.compareTo(secondRange.end) <= 0;
    const endIsAboveEnd: boolean = firstRange.end.compareTo(secondRange.end) >= 0;
    const endIsAboveStart: boolean = firstRange.end.compareTo(secondRange.start) >= 0;
    const isInRange: boolean = this.isInRange(firstRange, secondRange);

    if ((startIsBelowStart === true && endIsAboveStart === true) || (startIsBelowEnd === true && endIsAboveEnd) || isInRange) {
      return true;
    }

    return false;
  }
}
