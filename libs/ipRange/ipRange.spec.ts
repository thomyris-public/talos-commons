import { IpRange } from './ipRange';
import { ERROR } from '../enums/error';
import { Address } from '../address/address';

describe('IpRange', () => {
  it('Should create an IpRange', () => {
    // arrange
    // act
    const range: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
    // assert
    expect(range.start).toStrictEqual(new Address('10.0.0.1'));
    expect(range.end).toStrictEqual(new Address('10.0.0.20'));
  });

  describe('#cutRange', () => {
    it('Should return the base range because no excludedRanges given', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [];

      const expected: IpRange[] = [baseRange];
      // act
      const result: IpRange[] = baseRange.cutRange(excludedRanges);
      // assert
      expect(result).toStrictEqual(expected);
    });

    it('Should return a splited range', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [new IpRange({ start: '10.0.0.10', end: '10.0.0.15' })];

      const expected: IpRange[] = [
        new IpRange({
          start: '10.0.0.1',
          end: '10.0.0.9'
        }),
        new IpRange({
          start: '10.0.0.16',
          end: '10.0.0.20'
        })
      ];
      // act
      const result: IpRange[] = baseRange.cutRange(excludedRanges);
      // assert
      expect(result).toStrictEqual(expected);
    });

    it('Should return a splited range with on containing only one address', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [new IpRange({ start: '10.0.0.2', end: '10.0.0.4' })];

      const expected: IpRange[] = [
        new IpRange({
          start: '10.0.0.1',
          end: '10.0.0.1'
        }),
        new IpRange({
          start: '10.0.0.5',
          end: '10.0.0.20'
        })
      ];
      // act
      const result: IpRange[] = baseRange.cutRange(excludedRanges);
      // assert
      expect(result).toStrictEqual(expected);
    });

    it('Should return a splited range splited by multiple range', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [
        new IpRange({ start: '10.0.0.3', end: '10.0.0.5' }),
        new IpRange({ start: '10.0.0.10', end: '10.0.0.15' })
      ];

      const expected: IpRange[] = [
        new IpRange({
          start: '10.0.0.1',
          end: '10.0.0.2'
        }),
        new IpRange({
          start: '10.0.0.6',
          end: '10.0.0.9'
        }),
        new IpRange({
          start: '10.0.0.16',
          end: '10.0.0.20'
        })
      ];
      // act
      const result: IpRange[] = baseRange.cutRange(excludedRanges);
      // assert
      expect(result).toStrictEqual(expected);
    });

    it('Should return a splited range splited by multiple range in reverse order', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [
        new IpRange({ start: '10.0.0.10', end: '10.0.0.15' }),
        new IpRange({ start: '10.0.0.3', end: '10.0.0.5' })
      ];
      const expected: IpRange[] = [
        new IpRange({
          start: '10.0.0.1',
          end: '10.0.0.2'
        }),
        new IpRange({
          start: '10.0.0.6',
          end: '10.0.0.9'
        }),
        new IpRange({
          start: '10.0.0.16',
          end: '10.0.0.20'
        })
      ];
      // act
      const result: IpRange[] = baseRange.cutRange(excludedRanges);
      // assert
      expect(result).toStrictEqual(expected);
    });

    it('Should return an empty array because we cut with the same range', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [baseRange];
      const expected: IpRange[] = [];
      // act
      const result: IpRange[] = baseRange.cutRange(excludedRanges);
      // assert
      expect(result).toStrictEqual(expected);
    });

    it('Should return an empty array because we cut with multiples range wich are equal to the base range', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [
        new IpRange({ start: '10.0.0.1', end: '10.0.0.10' }),
        new IpRange({ start: '10.0.0.11', end: '10.0.0.20' })
      ];
      const expected: IpRange[] = [];
      // act
      const result: IpRange[] = baseRange.cutRange(excludedRanges);
      // assert
      expect(result).toStrictEqual(expected);
    });

    it('Should return only one range in the array left side', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [new IpRange({ start: '10.0.0.5', end: '10.0.0.20' })];
      const expected: IpRange[] = [new IpRange({ start: '10.0.0.1', end: '10.0.0.4' })];
      // act
      const result: IpRange[] = baseRange.cutRange(excludedRanges);
      // assert
      expect(result).toStrictEqual(expected);
    });

    it('Should return only one range in the array right side', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [new IpRange({ start: '10.0.0.1', end: '10.0.0.10' })];
      const expected: IpRange[] = [new IpRange({ start: '10.0.0.11', end: '10.0.0.20' })];
      // act
      const result: IpRange[] = baseRange.cutRange(excludedRanges);
      // assert
      expect(result).toStrictEqual(expected);
    });

    it('Should throw RANGE_OUTSIDE', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [new IpRange({ start: '10.0.0.10', end: '10.0.0.21' })];
      // act
      let result;
      try {
        baseRange.cutRange(excludedRanges);
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error(ERROR.RANGE_OUTSIDE));
    });

    it('Should throw RANGE_OUTSIDE', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [new IpRange({ start: '10.0.0.0', end: '10.0.0.19' })];
      // act
      let result;
      try {
        baseRange.cutRange(excludedRanges);
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error(ERROR.RANGE_OUTSIDE));
    });

    it('Should throw RANGE_OVERSIZED', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [new IpRange({ start: '9.0.0.2', end: '11.0.0.19' })];
      // act
      let result;
      try {
        baseRange.cutRange(excludedRanges);
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error(ERROR.RANGE_OVERSIZED));
    });

    it('Should throw RANGE_OVERLAP', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [
        new IpRange({ start: '10.0.0.2', end: '10.0.0.5' }),
        new IpRange({ start: '10.0.0.2', end: '10.0.0.5' })
      ];
      // act
      let result;
      try {
        baseRange.cutRange(excludedRanges);
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error(ERROR.RANGE_OVERLAP));
    });

    it('Should throw RANGE_OVERLAP for end above', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [
        new IpRange({ start: '10.0.0.2', end: '10.0.0.5' }),
        new IpRange({ start: '10.0.0.2', end: '10.0.0.6' })
      ];
      // act
      let result: unknown;
      try {
        baseRange.cutRange(excludedRanges);
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error(ERROR.RANGE_OVERLAP));
    });

    it('Should throw RANGE_OVERLAP for start and end below', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [
        new IpRange({ start: '10.0.0.2', end: '10.0.0.5' }),
        new IpRange({ start: '10.0.0.1', end: '10.0.0.4' })
      ];
      // act
      let result: unknown;
      try {
        baseRange.cutRange(excludedRanges);
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error(ERROR.RANGE_OVERLAP));
    });

    it('Should throw RANGE_OVERLAP for start and end below from second excluded range', () => {
      // arrange
      const baseRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const excludedRanges: IpRange[] = [
        new IpRange({ start: '10.0.0.5', end: '10.0.0.10' }),
        new IpRange({ start: '10.0.0.1', end: '10.0.0.11' })
      ];
      // act
      let result: unknown;
      try {
        baseRange.cutRange(excludedRanges);
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error(ERROR.RANGE_OVERLAP));
    });
  });

  describe('#checkRange', () => {
    it('Should return true', () => {
      // arrange
      const firstRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const secondRange: IpRange = new IpRange({ start: '10.0.0.2', end: '10.0.0.3' });
      // act
      const result: boolean = IpRange.checkRange(firstRange, secondRange);
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should throw RANGE_OUTSIDE', () => {
      // arrange
      const firstRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const secondRange: IpRange = new IpRange({ start: '10.0.0.10', end: '10.0.0.21' });
      // act
      let result: unknown;
      try {
        IpRange.checkRange(firstRange, secondRange);
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error(ERROR.RANGE_OUTSIDE));
    });

    it('Should throw RANGE_OUTSIDE', () => {
      // arrange
      const firstRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const secondRange: IpRange = new IpRange({ start: '10.0.0.0', end: '10.0.0.19' });
      // act
      let result: unknown;
      try {
        IpRange.checkRange(firstRange, secondRange);
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error(ERROR.RANGE_OUTSIDE));
    });

    it('Should throw RANGE_OVERSIZED', () => {
      // arrange
      const firstRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const secondRange: IpRange = new IpRange({ start: '9.0.0.2', end: '11.0.0.19' });
      // act
      let result: unknown;
      try {
        IpRange.checkRange(firstRange, secondRange);
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error(ERROR.RANGE_OVERSIZED));
    });
  });

  describe('#isInRange', () => {
    it('Should return true', () => {
      // arrange
      const firstRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const secondRange: IpRange = new IpRange({ start: '10.0.0.2', end: '10.0.0.3' });
      // act
      const result: boolean = IpRange.isInRange(firstRange, secondRange);
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false if end is out', () => {
      // arrange
      const firstRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const secondRange: IpRange = new IpRange({ start: '10.0.0.2', end: '10.0.0.21' });
      // act
      const result: boolean = IpRange.isInRange(firstRange, secondRange);
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false if start is out', () => {
      // arrange
      const firstRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.20' });
      const secondRange: IpRange = new IpRange({ start: '9.0.0.2', end: '10.0.0.10' });
      // act
      const result: boolean = IpRange.isInRange(firstRange, secondRange);
      // assert
      expect(result).toStrictEqual(false);
    });
  });

  describe('#isOverlaping', () => {
    it('Should return false', () => {
      // arrange
      const firstRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.10' });
      const secondRange: IpRange = new IpRange({ start: '10.0.0.11', end: '10.0.0.15' });
      // act
      const result: boolean = IpRange.isOverlaping(firstRange, secondRange);
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return true', () => {
      // arrange
      const firstRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.10' });
      const secondRange: IpRange = new IpRange({ start: '10.0.0.0', end: '10.0.0.8' });
      // act
      const result: boolean = IpRange.isOverlaping(firstRange, secondRange);
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true', () => {
      // arrange
      const firstRange: IpRange = new IpRange({ start: '10.0.0.1', end: '10.0.0.10' });
      const secondRange: IpRange = new IpRange({ start: '10.0.0.5', end: '10.0.0.11' });
      // act
      const result: boolean = IpRange.isOverlaping(firstRange, secondRange);
      // assert
      expect(result).toStrictEqual(true);
    });
  });
});
