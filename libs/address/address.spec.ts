import { Address4 } from 'ip-address';

import { Address } from './address';

describe('Address', () => {
  afterEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  describe('constructor', () => {
    describe('inputs', () => {
      it('should create with a valid ip in string', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/24');
        // Assert
        expect(address).toBeTruthy();
      });

      it('should create with a valid ip in Address4', () => {
        // Arrange
        // Act
        const address = new Address(new Address4('10.0.0.1/24'));
        // Assert
        expect(address).toBeTruthy();
      });

      it('should create with a valid ip and mask in string', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1', '24');
        // Assert
        expect(address).toBeTruthy();
      });

      it('should create with a valid ip and mask in number', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1', 24);
        // Assert
        expect(address).toBeTruthy();
      });

      it('Should throw an error because is not a valid address', () => {
        // arrange
        // act
        let result;
        try {
          result = new Address(new Address4('not'));
        } catch (error) {
          result = error;
        }
        // assert
        expect(result).toStrictEqual(new Error('ADDRESS_FORMAT_ERROR'));
      });

      it('Should throw an error because is not a valid address', () => {
        // arrange
        // act
        let result;
        try {
          result = new Address('not');
        } catch (error) {
          result = error;
        }
        // assert
        expect(result).toStrictEqual(new Error('ADDRESS_FORMAT_ERROR'));
      });

      it('Should throw an error because is not a valid address with wrong mask range string', () => {
        // arrange
        // act
        let result;
        try {
          result = new Address('10.0.0.1', '35');
        } catch (error) {
          result = error;
        }
        // assert
        expect(result).toStrictEqual(new Error('ADDRESS_FORMAT_ERROR'));
      });

      it('Should throw an error because is not a valid address with wrong mask', () => {
        // arrange
        // act
        let result;
        try {
          result = new Address('10.0.0.1', 'not');
        } catch (error) {
          result = error;
        }
        // assert
        expect(result).toStrictEqual(new Error('ADDRESS_FORMAT_ERROR'));
      });

      it('Should throw an error because is not a valid address with wrong mask number', () => {
        // arrange
        // act
        let result;
        try {
          result = new Address('10.0.0.1', -1);
        } catch (error) {
          result = error;
        }
        // assert
        expect(result).toStrictEqual(new Error('ADDRESS_FORMAT_ERROR'));
      });
    });

    describe('address', () => {
      it('should return 10.0.0.1/24 for 10.0.0.1/24', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/24');
        // Assert
        expect(address.address).toStrictEqual('10.0.0.1/24');
      });

      it('should return 10.0.0.2 for 10.0.0.2', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.2');
        // Assert
        expect(address.address).toStrictEqual('10.0.0.2');
      });

      it('should return 10.0.0.1 from new Address4(10.0.0.1)', () => {
        // Arrange
        // Act
        const address = new Address(new Address4('10.0.0.1'));
        // Assert
        expect(address.address).toStrictEqual('10.0.0.1');
      });
    });

    describe('ip', () => {
      it('should have ip 10.0.0.1 for 10.0.0.1/24', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/24');
        // Assert
        expect(address.ip).toStrictEqual('10.0.0.1');
      });

      it('should have ip 10.0.0.1 for 10.0.0.1', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1');
        // Assert
        expect(address.ip).toStrictEqual('10.0.0.1');
      });

      it('should have ip 10.0.0.100 for 10.0.0.100/26', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.100/26');
        // Assert
        expect(address.ip).toStrictEqual('10.0.0.100');
      });
    });

    describe('bigInteger', () => {
      it('should return 167772161 for 10.0.0.1/24', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/24');
        // Assert
        expect(address.bigInteger.toString()).toStrictEqual('167772161');
      });

      it('should return 167772162 for 10.0.0.2', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.2');
        // Assert
        expect(address.bigInteger.toString()).toStrictEqual('167772162');
      });

      it('should 10.0.0.1 minor 10.0.0.5', () => {
        // Arrange
        // Act
        const address1 = new Address('10.0.0.1');
        const address2 = new Address('10.0.0.5');
        // Assert
        expect(address1.bigInteger.compareTo(address2.bigInteger)).toBeLessThan(0);
      });
    });

    describe('network', () => {
      it('should have a network 10.0.0.0/24 for 10.0.0.1/24', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/24');
        // Assert
        expect(address.network).toStrictEqual('10.0.0.0/24');
      });

      it('should have a network 10.52.43.24/29 for 10.52.43.25/29', () => {
        // Arrange
        // Act
        const address = new Address('10.52.43.25/29');
        // Assert
        expect(address.network).toStrictEqual('10.52.43.24/29');
      });

      it('should have a network 10.52.0.0/18 for 10.52.43.25/18', () => {
        // Arrange
        // Act
        const address = new Address('10.52.43.25/18');
        // Assert
        expect(address.network).toStrictEqual('10.52.0.0/18');
      });

      it('should have a network 0.0.0.64/26 for 0.0.0.100/26', () => {
        // Arrange
        // Act
        const address = new Address('0.0.0.100/26');
        // Assert
        expect(address.network).toStrictEqual('0.0.0.64/26');
      });

      it('should have a network 192.168.1.42/32 for 192.168.1.42', () => {
        // Arrange
        // Act
        const address = new Address('192.168.1.42');
        // Assert
        expect(address.network).toStrictEqual('192.168.1.42/32');
      });

      it('should have a network 192.168.1.42/32 for 192.168.1.42/32', () => {
        // Arrange
        // Act
        const address = new Address('192.168.1.42/32');
        // Assert
        expect(address.network).toStrictEqual('192.168.1.42/32');
      });
    });

    describe('isNetwork', () => {
      it('should return true for 10.0.0.0/24', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/24');
        // Assert
        expect(address.isNetwork).toBeTruthy();
      });

      it('should return false for 10.52.43.25/29', () => {
        // Arrange
        // Act
        const address = new Address('10.52.43.25/29');
        // Assert
        expect(address.isNetwork).toBeFalsy();
      });

      it('should return true for 10.0.0.64/26', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.64/26');
        // Assert
        expect(address.isNetwork).toBeTruthy();
      });

      it('should return true for 10.0.0.1', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1');
        // Assert
        expect(address.isNetwork).toBeFalsy();
      });
    });

    describe('networkSize', () => {
      it('should return 1 for 10.0.0.0/32', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/32');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('1');
      });

      it('should return 2 for 10.0.0.0/31', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/31');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('2');
      });

      it('should return 2 for 10.0.0.0/30', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/30');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('2');
      });

      it('should return 6 for 10.52.43.25/29', () => {
        // Arrange
        // Act
        const address = new Address('10.52.43.25/29');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('6');
      });

      it('should return 14 for 10.52.43.25/28', () => {
        // Arrange
        // Act
        const address = new Address('10.52.43.25/28');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('14');
      });

      it('should return 30 for 10.52.43.25/27', () => {
        // Arrange
        // Act
        const address = new Address('10.52.43.25/27');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('30');
      });

      it('should return 62 for 10.0.0.64/26', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.64/26');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('62');
      });

      it('should return 126 for 10.0.0.0/25', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/25');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('126');
      });

      it('should return 254 for 10.0.0.0/24', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/24');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('254');
      });

      it('should return 510 for 10.0.0.0/23', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/23');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('510');
      });

      it('should return 1022 for 10.0.0.0/22', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/22');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('1022');
      });

      it('should return 2046 for 10.0.0.0/21', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/21');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('2046');
      });

      it('should return 4094 for 10.0.0.0/20', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/20');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('4094');
      });

      it('should return 8190 for 10.0.0.0/19', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/19');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('8190');
      });

      it('should return 16382 for 10.0.0.0/18', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/18');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('16382');
      });

      it('should return 32766 for 10.0.0.0/17', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/17');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('32766');
      });

      it('should return 65534 for 10.0.0.0/16', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/16');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('65534');
      });

      it('should return 131070 for 10.0.0.0/15', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/15');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('131070');
      });

      it('should return 262142 for 10.0.0.0/14', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/14');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('262142');
      });

      it('should return 524286 for 10.0.0.0/13', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/13');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('524286');
      });

      it('should return 1048574 for 10.0.0.0/12', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/12');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('1048574');
      });

      it('should return 2097150 for 10.0.0.0/11', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/11');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('2097150');
      });

      it('should return 4194302 for 10.0.0.0/10', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/10');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('4194302');
      });

      it('should return 8388606 for 10.0.0.0/9', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/9');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('8388606');
      });

      it('should return 16777214 for 10.0.0.0/8', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/8');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('16777214');
      });

      it('should return 33554430 for 10.0.0.0/7', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/7');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('33554430');
      });

      it('should return 67108862 for 10.0.0.0/6', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/6');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('67108862');
      });

      it('should return 134217726 for 10.0.0.0/5', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/5');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('134217726');
      });

      it('should return 268435454 for 10.0.0.0/4', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/4');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('268435454');
      });

      it('should return 536870910 for 10.0.0.0/3', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/3');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('536870910');
      });

      it('should return 1073741822 for 10.0.0.0/2', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/2');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('1073741822');
      });

      it('should return 2147483646 for 10.0.0.0/1', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/1');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('2147483646');
      });

      it('should return 4294967294 for 10.0.0.0/0', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.0/0');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('4294967294');
      });

      it('should return 1 for 10.0.0.1', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1');
        // Assert
        expect(address.networkSize.toString()).toStrictEqual('1');
      });
    });

    describe('mask', () => {
      it('should have mask 24 for 10.0.0.1/24', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/24');
        // Assert
        expect(address.mask).toStrictEqual(24);
      });

      it('should have mask 32 for 10.0.0.1', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1');
        // Assert
        expect(address.mask).toStrictEqual(32);
      });
    });

    describe('subnetMask', () => {
      it('should return 255.255.255.255 for 10.0.0.1/32', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/32');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.255.255');
      });

      it('should return 255.255.255.254 for 10.0.0.1/31', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/31');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.255.254');
      });

      it('should return 255.255.255.252 for 10.0.0.1/30', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/30');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.255.252');
      });

      it('should return 255.255.255.248 for 10.0.0.1/29', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/29');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.255.248');
      });

      it('should return 255.255.255.240 for 10.0.0.1/28', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/28');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.255.240');
      });

      it('should return 255.255.255.224 for 10.0.0.1/27', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/27');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.255.224');
      });

      it('should return 255.255.255.192 for 10.0.0.1/26', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/26');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.255.192');
      });

      it('should return 255.255.255.128 for 10.0.0.1/25', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/25');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.255.128');
      });

      it('should return 255.255.255.0 for 10.0.0.1/24', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/24');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.255.0');
      });

      it('should return 255.255.254.0 for 10.0.0.1/23', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/23');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.254.0');
      });

      it('should return 255.255.252.0 for 10.0.0.1/22', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/22');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.252.0');
      });

      it('should return 255.255.248.0 for 10.0.0.1/21', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/21');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.248.0');
      });

      it('should return 255.255.240.0 for 10.0.0.1/20', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/20');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.240.0');
      });

      it('should return 255.255.224.0 for 10.0.0.1/19', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/19');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.224.0');
      });

      it('should return 255.255.192.0 for 10.0.0.1/18', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/18');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.192.0');
      });

      it('should return 255.255.128.0 for 10.0.0.1/17', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/17');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.128.0');
      });

      it('should return 255.255.0.0 for 10.0.0.1/16', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/16');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.0.0');
      });

      it('should return 255.254.0.0 for 10.0.0.1/15', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/15');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.254.0.0');
      });

      it('should return 255.252.0.0 for 10.0.0.1/14', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/14');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.252.0.0');
      });

      it('should return 255.248.0.0 for 10.0.0.1/13', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/13');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.248.0.0');
      });

      it('should return 255.240.0.0 for 10.0.0.1/12', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/12');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.240.0.0');
      });

      it('should return 255.224.0.0 for 10.0.0.1/11', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/11');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.224.0.0');
      });

      it('should return 255.192.0.0 for 10.0.0.1/10', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/10');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.192.0.0');
      });

      it('should return 255.128.0.0 for 10.0.0.1/9', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/9');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.128.0.0');
      });

      it('should return 255.0.0.0 for 10.0.0.1/8', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/8');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.0.0.0');
      });

      it('should return 254.0.0.0 for 10.0.0.1/7', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/7');
        // Assert
        expect(address.subnetMask).toStrictEqual('254.0.0.0');
      });

      it('should return 252.0.0.0 for 10.0.0.1/6', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/6');
        // Assert
        expect(address.subnetMask).toStrictEqual('252.0.0.0');
      });

      it('should return 248.0.0.0 for 10.0.0.1/5', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/5');
        // Assert
        expect(address.subnetMask).toStrictEqual('248.0.0.0');
      });

      it('should return 240.0.0.0 for 10.0.0.1/4', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/4');
        // Assert
        expect(address.subnetMask).toStrictEqual('240.0.0.0');
      });

      it('should return 224.0.0.0 for 10.0.0.1/3', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/3');
        // Assert
        expect(address.subnetMask).toStrictEqual('224.0.0.0');
      });

      it('should return 192.0.0.0 for 10.0.0.1/2', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/2');
        // Assert
        expect(address.subnetMask).toStrictEqual('192.0.0.0');
      });

      it('should return 128.0.0.0 for 10.0.0.1/1', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/1');
        // Assert
        expect(address.subnetMask).toStrictEqual('128.0.0.0');
      });

      it('should return 0.0.0.0 for 10.0.0.1/0', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/0');
        // Assert
        expect(address.subnetMask).toStrictEqual('0.0.0.0');
      });

      it('should return 255.255.255.255 for 10.0.0.1', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1');
        // Assert
        expect(address.subnetMask).toStrictEqual('255.255.255.255');
      });
    });

    describe('wildcardMask', () => {
      it('should return 0.0.0.0 for 10.0.0.1/32', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/32');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.0.0');
      });

      it('should return 0.0.0.1 for 10.0.0.1/31', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/31');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.0.1');
      });

      it('should return 0.0.0.3 for 10.0.0.1/30', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/30');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.0.3');
      });

      it('should return 0.0.0.7 for 10.0.0.1/29', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/29');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.0.7');
      });

      it('should return 0.0.0.15 for 10.0.0.1/28', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/28');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.0.15');
      });

      it('should return 0.0.0.31 for 10.0.0.1/27', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/27');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.0.31');
      });

      it('should return 0.0.0.63 for 10.0.0.1/26', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/26');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.0.63');
      });

      it('should return 0.0.0.127 for 10.0.0.1/25', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/25');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.0.127');
      });

      it('should return 0.0.0.255 for 10.0.0.1/24', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/24');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.0.255');
      });

      it('should return 0.0.1.255 for 10.0.0.1/23', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/23');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.1.255');
      });

      it('should return 0.0.3.255 for 10.0.0.1/22', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/22');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.3.255');
      });

      it('should return 0.0.7.255 for 10.0.0.1/21', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/21');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.7.255');
      });

      it('should return 0.0.15.255 for 10.0.0.1/20', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/20');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.15.255');
      });

      it('should return 0.0.31.255 for 10.0.0.1/19', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/19');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.31.255');
      });

      it('should return 0.0.63.255 for 10.0.0.1/18', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/18');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.63.255');
      });

      it('should return 0.0.127.255 for 10.0.0.1/17', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/17');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.127.255');
      });

      it('should return 0.0.255.255 for 10.0.0.1/16', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/16');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.255.255');
      });

      it('should return 0.1.255.255 for 10.0.0.1/15', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/15');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.1.255.255');
      });

      it('should return 0.3.255.255 for 10.0.0.1/14', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/14');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.3.255.255');
      });

      it('should return 0.7.255.255 for 10.0.0.1/13', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/13');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.7.255.255');
      });

      it('should return 0.15.255.255 for 10.0.0.1/12', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/12');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.15.255.255');
      });

      it('should return 0.31.255.255 for 10.0.0.1/11', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/11');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.31.255.255');
      });

      it('should return 0.63.255.255 for 10.0.0.1/10', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/10');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.63.255.255');
      });

      it('should return 0.127.255.255 for 10.0.0.1/9', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/9');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.127.255.255');
      });

      it('should return 0.255.255.255 for 10.0.0.1/8', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/8');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.255.255.255');
      });

      it('should return 1.255.255.255 for 10.0.0.1/7', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/7');
        // Assert
        expect(address.wildcardMask).toStrictEqual('1.255.255.255');
      });

      it('should return 3.255.255.255 for 10.0.0.1/6', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/6');
        // Assert
        expect(address.wildcardMask).toStrictEqual('3.255.255.255');
      });

      it('should return 7.255.255.255 for 10.0.0.1/5', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/5');
        // Assert
        expect(address.wildcardMask).toStrictEqual('7.255.255.255');
      });

      it('should return 15.255.255.255 for 10.0.0.1/4', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/4');
        // Assert
        expect(address.wildcardMask).toStrictEqual('15.255.255.255');
      });

      it('should return 31.255.255.255 for 10.0.0.1/3', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/3');
        // Assert
        expect(address.wildcardMask).toStrictEqual('31.255.255.255');
      });

      it('should return 63.255.255.255 for 10.0.0.1/2', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/2');
        // Assert
        expect(address.wildcardMask).toStrictEqual('63.255.255.255');
      });

      it('should return 127.255.255.255 for 10.0.0.1/1', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/1');
        // Assert
        expect(address.wildcardMask).toStrictEqual('127.255.255.255');
      });

      it('should return 255.255.255.255 for 10.0.0.1/0', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/0');
        // Assert
        expect(address.wildcardMask).toStrictEqual('255.255.255.255');
      });

      it('should return 0.0.0.0 for 10.0.0.1', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1');
        // Assert
        expect(address.wildcardMask).toStrictEqual('0.0.0.0');
      });
    });

    describe('broadcast', () => {
      it('should return 10.0.0.255 for 10.0.0.1/24', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/24');
        // Assert
        expect(address.broadcast).toStrictEqual('10.0.0.255');
      });

      it('should return 10.0.0.127 for 10.0.0.100/26', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.100/26');
        // Assert
        expect(address.broadcast).toStrictEqual('10.0.0.127');
      });

      it('should return 10.0.0.1 for 10.0.0.1', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1');
        // Assert
        expect(address.broadcast).toStrictEqual('10.0.0.1');
      });
    });

    describe('isBroadcast', () => {
      it('should return true for 10.0.0.255', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.255');
        // Assert
        expect(address.isBroadcast).toBeTruthy();
      });

      it('should return true for 10.0.0.255/32', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.255/32');
        // Assert
        expect(address.isBroadcast).toBeTruthy();
      });

      it('should return false for 10.0.0.1/24', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.1/24');
        // Assert
        expect(address.isBroadcast).toBeFalsy();
      });

      it('should return true for 10.0.0.127/26', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.127/26');
        // Assert
        expect(address.isBroadcast).toBeTruthy();
      });
    });

    describe('firstHost', () => {
      it('should return 192.168.1.1 for 192.168.1.100/24', () => {
        // Arrange
        // Act
        const address = new Address('192.168.1.100/24');
        // Assert
        expect(address.firstHost).toStrictEqual('192.168.1.1');
      });

      it('should return true for 10.0.0.250/32', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.250/32');
        // Assert
        expect(address.firstHost).toStrictEqual('10.0.0.250');
      });
    });

    describe('lastHost', () => {
      it('should return 192.168.1.1 for 192.168.1.100/24', () => {
        // Arrange
        // Act
        const address = new Address('192.168.1.100/24');
        // Assert
        expect(address.lastHost).toStrictEqual('192.168.1.254');
      });

      it('should return 10.0.0.250 for 10.0.0.250/32', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.250/32');
        // Assert
        expect(address.lastHost).toStrictEqual('10.0.0.250');
      });
    });

    describe('isHost', () => {
      it('should return true for 192.168.1.100', () => {
        // Arrange
        // Act
        const address = new Address('192.168.1.100');
        // Assert
        expect(address.isHost).toBeTruthy();
      });

      it('should return true for 10.0.0.250/32', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.250/32');
        // Assert
        expect(address.isHost).toBeTruthy();
      });

      it('should return false for 10.0.0.250/24', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.250/24');
        // Assert
        expect(address.isHost).toBeFalsy();
      });

      it('should return false for 0.0.0.0/0', () => {
        // Arrange
        // Act
        const address = new Address('0.0.0.0/0');
        // Assert
        expect(address.isHost).toBeFalsy();
      });
    });

    describe('isAll', () => {
      it('should return false for 192.168.1.100', () => {
        // Arrange
        // Act
        const address = new Address('192.168.1.100');
        // Assert
        expect(address.isAll).toBeFalsy();
      });

      it('should return false for 10.0.0.250/32', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.250/32');
        // Assert
        expect(address.isAll).toBeFalsy();
      });

      it('should return true for 10.0.0.250/24', () => {
        // Arrange
        // Act
        const address = new Address('10.0.0.250/24');
        // Assert
        expect(address.isAll).toBeFalsy();
      });

      it('should return true for 0.0.0.0/0', () => {
        // Arrange
        // Act
        const address = new Address('0.0.0.0/0');
        // Assert
        expect(address.isAll).toBeTruthy();
      });

      it('should return true for 0.0.0.0', () => {
        // Arrange
        // Act
        const address = new Address('0.0.0.0');
        // Assert
        expect(address.isAll).toBeTruthy();
      });

      it('should return true for 0.0.0.1/0', () => {
        // Arrange
        // Act
        const address = new Address('0.0.0.1/0');
        // Assert
        expect(address.isAll).toBeTruthy();
      });
    });
  });

  describe('toString', () => {
    it('should return a string correct', () => {
      // Arrange
      // Act
      const address = new Address('10.0.0.1/24');
      // Assert
      expect(address.toString()).toStrictEqual('10.0.0.1/24');
      expect(address + '').toStrictEqual('10.0.0.1/24');
    });
  });

  describe('compareTo', () => {
    it('should return inferior', () => {
      // Arrange
      const address1 = new Address('10.0.0.1');
      const address2 = new Address('10.0.0.42');
      // Act
      const result = address1.compareTo(address2);
      // Assert
      expect(result).toBeLessThan(0);
    });

    it('should return equal', () => {
      // Arrange
      const address1 = new Address('10.0.0.1');
      const address2 = new Address('10.0.0.1');
      // Act
      const result = address1.compareTo(address2);
      // Assert
      expect(result).toStrictEqual(0);
    });

    it('should return superior', () => {
      // Arrange
      const address1 = new Address('10.0.0.45');
      const address2 = new Address('10.0.0.1');
      // Act
      const result = address1.compareTo(address2);
      // Assert
      expect(result).toBeGreaterThan(0);
    });

    it('should return inferior with network', () => {
      // Arrange
      const address1 = new Address('10.0.0.100/26'); // fist host 10.0.0.65
      const address2 = new Address('10.0.0.66');
      // Act
      const result = address1.compareTo(address2);
      // Assert
      expect(result).toBeLessThan(0);
    });

    it('should return inferior with network', () => {
      // Arrange
      const address1 = new Address('10.0.0.1/26'); // fist host 10.0.0.65
      const address2 = new Address('10.0.0.100/26');
      // Act
      const result = address1.compareTo(address2);
      // Assert
      expect(result).toBeLessThan(0);
    });
  });

  describe('contain', () => {
    it('should return no jonction for 10.0.0.1/24 contain 10.0.1.1/24 ?', () => {
      // Arrange
      const address1 = new Address('10.0.0.1/24'); // 10.0.0.1-10.0.1.254
      const address2 = new Address('10.0.1.1/24'); // 10.0.1.1-10.1.1.254
      // Act
      const result = address1.contain(address2);
      // Assert
      expect(result).toStrictEqual(0);
    });

    it('should return 1 for 10.0.0.1/24 contain 10.0.0.1/26 ?', () => {
      // Arrange
      const address1 = new Address('10.0.0.100/24'); // 10.0.0.1-10.0.0.254
      const address2 = new Address('10.0.0.100/26'); // 10.0.0.65-10.0.0.126
      // Act
      const result = address1.contain(address2);
      // Assert
      expect(result).toStrictEqual(1);
    });

    it('should return 1 for 10.0.0.1/24 contain 10.0.0.0/24 ?', () => {
      // Arrange
      const address1 = new Address('10.0.0.1/24'); // 10.0.0.1-10.0.0.254
      const address2 = new Address('10.0.0.0/24'); // 10.0.0.1-10.0.0.254
      // Act
      const result = address1.contain(address2);
      // Assert
      expect(result).toStrictEqual(1);
    });

    it('should return -1 for 10.0.0.1/26 contain 10.0.0.1/24 ?', () => {
      // Arrange
      const address1 = new Address('10.0.0.100/26'); // 10.0.0.65-10.0.0.126
      const address2 = new Address('10.0.0.100/24'); // 10.0.0.1-10.0.0.254
      // Act
      const result = address1.contain(address2);
      // Assert
      expect(result).toStrictEqual(-1);
    });
  });

  describe('isPrivate', () => {
    it('should return private for 10.0.0.1/24', () => {
      // Arrange
      const address = new Address('10.0.0.1/24');
      // Act
      const result = address.isPrivate();
      // Assert
      expect(result).toBeTruthy();
    });

    it('should return not private for 11.0.0.100/24', () => {
      // Arrange
      const address = new Address('11.0.0.100/24');
      // Act
      const result = address.isPrivate();
      // Assert
      expect(result).toBeFalsy();
    });

    it('should return private for 172.16.0.1/24', () => {
      // Arrange
      const address = new Address('172.16.0.1/24');
      // Act
      const result = address.isPrivate();
      // Assert
      expect(result).toBeTruthy();
    });

    it('should return private for 192.168.0.1/24', () => {
      // Arrange
      const address = new Address('192.168.0.1/24');
      // Act
      const result = address.isPrivate();
      // Assert
      expect(result).toBeTruthy();
    });
  });

  describe('isReserved', () => {
    it('should return not reserved for 10.0.0.1/24', () => {
      // Arrange
      const address = new Address('10.0.0.1/24');
      // Act
      const result = address.isReserved();
      // Assert
      expect(result).toBeFalsy();
    });

    it('should return reserved loopback for 127.0.0.1', () => {
      // Arrange
      const address = new Address('127.0.0.1');
      // Act
      const result = address.isReserved();
      // Assert
      expect(result).toBeTruthy();
    });

    it('should return reserved APIPA for 169.254.0.0/24', () => {
      // Arrange
      const address = new Address('169.254.0.0/24');
      // Act
      const result = address.isReserved();
      // Assert
      expect(result).toBeTruthy();
    });

    it('should return reserved multicast for 169.254.0.0/24', () => {
      // Arrange
      const address = new Address('169.254.0.0/24');
      // Act
      const result = address.isReserved();
      // Assert
      expect(result).toBeTruthy();
    });

    it('should return reserved cgn for 100.64.0.1', () => {
      // Arrange
      const address = new Address('100.64.0.1');
      // Act
      const result = address.isReserved();
      // Assert
      expect(result).toBeTruthy();
    });

    it('should throw an error ADDRESS_FORMAT_ERROR when the address is wrong "172.16.1.01"', () => {
      // arrange
      // act
      let result;
      try {
        result = new Address('172.16.1.01');
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error('ADDRESS_FORMAT_ERROR'));
    });

    it('should throw an error ADDRESS_FORMAT_ERROR when the address is wrong "010.0.0.0"', () => {
      // arrange
      // act
      let result;
      try {
        result = new Address('010.0.0.0');
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error('ADDRESS_FORMAT_ERROR'));
    });

    it('should throw an error ADDRESS_FORMAT_ERROR when the address is wrong "10.020.0.100"', () => {
      // arrange
      // act
      let result;
      try {
        result = new Address('10.020.0.100');
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error('ADDRESS_FORMAT_ERROR'));
    });

    it('should throw an error ADDRESS_FORMAT_ERROR when the address is wrong "90.0.050.0"', () => {
      // arrange
      // act
      let result;
      try {
        result = new Address('90.0.050.0');
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error('ADDRESS_FORMAT_ERROR'));
    });

    it('should throw an error ADDRESS_FORMAT_ERROR when the address is wrong "92.173.122.0100"', () => {
      // arrange
      // act
      let result;
      try {
        result = new Address('92.173.122.0100');
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error('ADDRESS_FORMAT_ERROR'));
    });

    it('should throw an error ADDRESS_FORMAT_ERROR when the address is wrong "10.0.00.1"', () => {
      // arrange
      // act
      let result;
      try {
        result = new Address('10.0.00.1');
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error('ADDRESS_FORMAT_ERROR'));
    });

    it('should throw an error ADDRESS_FORMAT_ERROR when the address is wrong "10.0.0.01/24"', () => {
      // arrange
      // act
      let result;
      try {
        result = new Address('10.0.0.01/24');
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error('ADDRESS_FORMAT_ERROR'));
    });
  });

  describe('isMulticast', () => {
    it('should return true', () => {
      // Arrange
      const address = new Address('224.0.0.1');
      // Act
      const result = address.isMulticast();
      // Assert
      expect(result).toStrictEqual(true);
    });

    it('should return false', () => {
      // Arrange
      const address = new Address('10.0.0.1/24');
      // Act
      const result = address.isMulticast();
      // Assert
      expect(result).toStrictEqual(false);
    });
  });

  describe('getFamily', () => {
    it('should return "host"', () => {
      // Arrange
      const address = new Address('10.1.0.1');
      // Act
      const result = address.getFamily();
      // Assert
      expect(result).toStrictEqual('Host');
    });

    it('should return "network"', () => {
      // Arrange
      const address = new Address('172.16.234.0/24');
      // Act
      const result = address.getFamily();
      // Assert
      expect(result).toStrictEqual('Network');
    });
  });

  describe('isInRange', () => {
    it('should return true when the address is in range', () => {
      // Arrange
      const address = new Address('10.1.0.10');
      const start = new Address('10.1.0.1');
      const end = new Address('10.1.0.30');
      // Act
      const result = address.isInRange(start, end);
      // Assert
      expect(result).toStrictEqual(true);
    });

    it('should return false when the address is below the range', () => {
      // Arrange
      const address = new Address('10.1.0.3');
      const start = new Address('10.1.0.7');
      const end = new Address('10.1.0.30');
      // Act
      const result = address.isInRange(start, end);
      // Assert
      expect(result).toStrictEqual(false);
    });

    it('should return false when the address is above the range', () => {
      // Arrange
      const address = new Address('10.1.0.50');
      const start = new Address('10.1.0.7');
      const end = new Address('10.1.0.30');
      // Act
      const result = address.isInRange(start, end);
      // Assert
      expect(result).toStrictEqual(false);
    });

    it('should throw an error when the address is not a host', () => {
      // Arrange
      const address = new Address('10.1.0.0/24');
      const start = new Address('10.1.0.7');
      const end = new Address('10.1.0.30');
      // Act
      let result;
      try {
        address.isInRange(start, end);
      } catch (e) {
        result = e;
      }
      // Assert
      expect(result).toStrictEqual(new Error(`Address [${address.address}] must have the type: Host`));
    });

    it('should throw an error when the start address is not a host', () => {
      // Arrange
      const address = new Address('10.1.0.50');
      const start = new Address('10.1.0.0/24');
      const end = new Address('10.1.0.30');
      // Act
      let result;
      try {
        address.isInRange(start, end);
      } catch (e) {
        result = e;
      }
      // Assert
      expect(result).toStrictEqual(new Error(`Address [${start.address}] must have the type: Host`));
    });

    it('should throw an error when the end address is not a host', () => {
      // Arrange
      const address = new Address('10.1.0.50');
      const start = new Address('10.1.0.1');
      const end = new Address('10.1.0.0/24');
      // Act
      let result;
      try {
        address.isInRange(start, end);
      } catch (e) {
        result = e;
      }
      // Assert
      expect(result).toStrictEqual(new Error(`Address [${end.address}] must have the type: Host`));
    });
  });
});
