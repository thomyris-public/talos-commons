import { BigInteger } from 'jsbn';
import { Address4 } from 'ip-address';

export class Address {
  /** @type {Address4} From package ip-address */
  private address4: Address4;

  /** @type {string} The address as string */
  address: string;

  /** @type {sring} The ip address in IP/MASK */
  ip: string;

  /** @type {number} The mask or CIDR 'Classless Inter-Domain Routing in IP/MASK */
  mask: number;

  /** @type {string} Network of the address as IP/MASK */
  network: string;

  /** @type {BigInteger} The network size without reserved address */
  networkSize: BigInteger;

  /** @type {boolean} If the address is equal to the network address */
  isNetwork: boolean;

  /** @type {boolean} If the address has mask 32 */
  isHost: boolean;

  /** @type {string} The subnetMask for the address (full minus size) */
  subnetMask: string;

  /** @type {string} The wildcardMask for the address (empty plus size) */
  wildcardMask: string;

  /** @type {string} The broadcast for the address */
  broadcast: string;

  /** @type {boolean} If the address is equal to the broadcast ip */
  isBroadcast: boolean;

  /** @type {BigInteger} The ip to bigInteger */
  bigInteger: BigInteger;

  /** @type {string} The fist host of network */
  firstHost: string;

  /** @type {string} The last host of network */
  lastHost: string;

  /** @type {boolean} If the address is all 0.0.0.0/0 */
  isAll: boolean;

  /**
   * Class for manipulate address IP with option MASK
   * @constructor
   * @param address ip as string, or address as string, or ip-address
   * @param mask optional mask as string or number
   */
  constructor(address: string | Address4, mask?: number | string) {
    this.address4 = address as Address4;
    this.address = address as string;

    if (typeof address === 'string' && mask) {
      this.address4 = new Address4(address + '/' + mask);
      if (!this.address4.valid) {
        throw new Error('ADDRESS_FORMAT_ERROR');
      }
    } else if (typeof address === 'string') {
      this.address4 = new Address4(address);
      if (!this.address4.valid) {
        throw new Error('ADDRESS_FORMAT_ERROR');
      }
      this.address = address;
    } else if (address instanceof Address4) {
      if (address.isValid()) {
        this.address4 = address;
        this.address = this.address4.address;
      } else {
        throw new Error('ADDRESS_FORMAT_ERROR');
      }
    }

    this.checkAddress(this.address4);

    // The ip to bigInteger
    this.bigInteger = new Address4(this.address4.addressMinusSuffix).bigInteger();

    // Network of the address like 10.0.0.1/24 => 10.0.0.0/24
    this.network = this.address4.startAddress().address + this.address4.subnet;

    // If the address is the network address
    this.isNetwork = this.address4.subnetMask !== 32 && this.address === this.network;

    // The network size without reserved address
    this.networkSize = this.getNetworkSize();

    // The mask for the address
    this.mask = this.address4.subnetMask;

    // If the address has mask 32
    this.isHost = this.address4.subnetMask === 32;

    // The ip address in IP/MASK
    this.ip = this.address4.addressMinusSuffix;

    // The subnetMask for the address
    this.subnetMask = this.getSubnetMask();

    // The wildcardMask for the address
    this.wildcardMask = this.getWildcardMask();

    // The broadcast for the address
    this.broadcast = this.address4.endAddress().address;

    // If the address is equal the broadcast address
    this.isBroadcast = this.address4.addressMinusSuffix === this.broadcast;

    // The fist host of network
    if (this.address4.subnetMask === 32) {
      this.firstHost = this.address4.addressMinusSuffix;
    } else {
      this.firstHost = this.address4.startAddressExclusive().address;
    }

    // The last host of network
    if (this.address4.subnetMask === 32) {
      this.lastHost = this.address4.addressMinusSuffix;
    } else {
      this.lastHost = this.address4.endAddressExclusive().address;
    }

    // If the address is all 0.0.0.0/0
    this.isAll = this.address4.bigInteger().equals(new BigInteger('0')) || this.address4.subnetMask === 0;
  }

  private checkAddress(address: Address4): void {
    for (const part of address.parsedAddress) {
      if (part.length > 1) {
        const numbers: number[] = part.split('').map(x => Number(x));
        if (numbers[0] === 0) throw new Error('ADDRESS_FORMAT_ERROR');
      }
    }
  }

  /**
   * Calcul the network size from the mask.
   *
   * @returns {BigInteger} The network size without reserved address
   */
  getNetworkSize(): BigInteger {
    const mask: number = this.address4.subnetMask;
    const maximumMask: BigInteger = new BigInteger('32');
    const maskSize: number = maximumMask.subtract(new BigInteger(mask.toString())).intValue();
    const networkSize: BigInteger = new BigInteger('2').pow(maskSize);

    if (mask !== 31 && mask !== 32) {
      return networkSize.subtract(new BigInteger('2'));
    }

    return networkSize;
  }

  /**
   * Calcul the subnet mask from the mask of the address.
   *
   * @returns {string} The subnet mask
   */
  getSubnetMask(): string {
    const mask: number = this.address4.subnetMask;
    const binaryMask: string = '1'.repeat(mask) + '0'.repeat(32 - mask);

    return Address4.fromBigInteger(new BigInteger(binaryMask, 2)).address;
  }

  /**
   * Calcul the wildcard mask from the mask of the address.
   *
   * @returns {string} The wildcard mask from the network size.
   */
  getWildcardMask(): string {
    const mask: number = this.address4.subnetMask;
    const binaryMask: string = '0'.repeat(mask) + '1'.repeat(32 - mask);

    return Address4.fromBigInteger(new BigInteger(binaryMask, 2)).address;
  }

  /**
   * To cast as string
   * @returns the address
   */
  toString(): string {
    return this.address4.address;
  }

  /**
   * Compare to an other address, if network use first address
   * return + if this > a, - if this < a, 0 if equal
   * @param otherAddress
   * @returns
   */
  compareTo(otherAddress: Address): number {
    let self: Address = this;
    if (this.mask !== 32) {
      self = new Address(this.firstHost);
    }
    let other: Address = otherAddress;
    if (other.mask !== 32) {
      other = new Address(other.firstHost);
    }
    return self.bigInteger.compareTo(other.bigInteger);
  }

  /**
   * Check to an other address
   * return -1 if other have this
   * return 0 if this don't have other
   * return 1 if this have other or equal
   * @param other
   * @returns
   */
  contain(other: Address): number {
    const thisNetwork = new Address(this.network);
    const otherNetwork = new Address(other.network);
    const seflFirst = new Address(thisNetwork.firstHost).bigInteger;
    const seflLast = new Address(thisNetwork.lastHost).bigInteger;
    const otherFirst = new Address(otherNetwork.firstHost).bigInteger;
    const otherLast = new Address(otherNetwork.lastHost).bigInteger;

    if (thisNetwork.address === otherNetwork.address) {
      return 1;
      // this contain other
    } else if (seflLast.compareTo(otherFirst) < 0) {
      return 0;
      // this contain other
    } else if (otherFirst.compareTo(seflFirst) >= 0 && otherLast.compareTo(seflLast) <= 0) {
      return 1;
      // other contain this
    } else if (seflFirst.compareTo(otherFirst) >= 0 && seflLast.compareTo(otherLast) <= 0) {
      return -1;
    }

    return 0;
  }

  /**
   * If the address is private
   * Not in private A 10.0.0.0/8
   * Not in private B 172.16.0.0/12
   * Not in private C 192.168.0.0/16
   * @returns
   */
  isPrivate(): boolean {
    const privateA = new Address('10.0.0.0/8');
    const privateB = new Address('172.16.0.0/12');
    const privateC = new Address('192.168.0.0/16');
    return privateA.contain(this) === 1 || privateB.contain(this) === 1 || privateC.contain(this) === 1;
  }

  /**
   * If the address is reserved
   * Not in loopback 127.0.0.0/8
   * Not in APIPA 169.254.0.0/16
   * Not in multicast 224.0.0.0/4
   * Not in CGN RFC6598 100.64.0.0/10
   * @returns
   */
  isReserved(): boolean {
    const loopBack = new Address('127.0.0.0/8');
    const APIPA = new Address('169.254.0.0/16');
    const multicast = new Address('224.0.0.0/4');
    const cgn = new Address('100.64.0.0/10');
    return loopBack.contain(this) === 1 || APIPA.contain(this) === 1 || multicast.contain(this) === 1 || cgn.contain(this) === 1;
  }

  /**
   * If is in multicast 224.0.0.0/4
   * @returns
   */
  isMulticast(): boolean {
    const multicast = new Address('224.0.0.0/4');
    return multicast.contain(this) === 1;
  }

  /**
   * Get the family of the address.
   *
   * @returns {string} - the address family.
   */
  getFamily(): string {
    if (this.isHost) return 'Host';
    return 'Network';
  }

  /**
   * Check if the host address is in the range of the other host addresses.
   *
   * @param {Address} start - the start address of the range.
   * @param {Address} end - the end address of the range.
   * @returns {boolean} - true if the host address is in the range.
   */
  isInRange(start: Address, end: Address): boolean {
    if (this.getFamily() !== 'Host') {
      throw new Error(`Address [${this.address}] must have the type: Host`);
    }
    if (start.getFamily() !== 'Host') {
      throw new Error(`Address [${start}] must have the type: Host`);
    }
    if (end.getFamily() !== 'Host') {
      throw new Error(`Address [${end}] must have the type: Host`);
    }

    const isAboveStart: boolean = this.compareTo(start) >= 0;
    const isBelowEnd: boolean = this.compareTo(end) <= 0;

    return isAboveStart && isBelowEnd;
  }
}
