export enum USER_ROLE {
  USER = 'USER',
  TECH = 'TECH',
  EXPERT = 'EXPERT',
  ADMIN = 'ADMIN',
  THOMYRIS = 'THOMYRIS'
}
