/**
* TEST
*/
export enum REGEX {
  /**
   * Corresponds to the code but for the login field.
   * Format:
   * ```js
   * "frtfezdefrfezhl478454545"
   * ```
   */
  USER_LOGIN = '^[a-z0-9]{4,24}$',

  /**
   * User code.
   * Format:
   * ```js
   * "ltho1"
   * ```
   */
  USER_CODE = '^([a-z]{5})(?:([1-9]{1})([0-9]{0,4}))?$',

  /**
   * Value of a user email address.
   * Format:
   * ```js
   * "lastnamefirstname@gmail.com"
   * ```
   */
  USER_MAIL = '^([\\w\\-\\.]{2,95}@([a-zA-Z]{2,95})\\.[\\w-]{2,65})$',

  /**
   * Last name or first name of a user.
   * Format:
   * ```js
   * "last name first name"
   * ```
   */
  USER_NAME = '^(?!\\s)([a-zA-Z-/\' à-żÀ-Ż]){2,48}(?<!\\s)$',

  /**
   * To remove the special Cr from the surname and first name. Must match the special cr of the USER_NAME regex.
   */
  SPECIAL_CHAR_USER_NAME = '[_/\\-\' à-żÀ-Ż]+',

  /**
   * User passwords.
   * Format:
   * ```js
   * "last name first name"
   * ```
   */
  PASSWORD = '^(?=\\S.*[a-zà-ż])(?=.*[A-ZÀ-Ż])(?=.*\\d)(?=.*[!@#$%&*()\\-_=+<>/|?.,;^¨])[A-Za-zà-żÀ-Ż\\d!@#$%&*()\\-_=+<>/|?.,;^¨]{6,50}$',

  /**
   * Name of an object (name ni, dhcp, organization).
   * Format:
   * ```js
   * "xefi-grenoble"
   * ```
   */
  OBJECT_NAME = '^(?!\\s)([a-zA-Z0-9-/\'_.@ à-żÀ-Ż]){3,50}(?<!\\s)$',

  /**
   * For a description field.
   * Format:
   * ```js
   * "Text"
   * ```
   */
  DESCRIPTION = '^[a-zA-Z0-9-/\',_.@ à-żÀ-Ż]{0,100}$',

  /**
   * For the name of a site-to-site vpn connection (parent and child).
   * Format: Numbers and letters
   */
  IKE_CONNECTION_NAME = '^([a-z0-9](?:-[a-z0-9])*){3,25}$',

  /**
   * For the name of a dns.
   * Format: Numbers and letters
   */
  DNS_NAME = '^(([a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9].)+[a-zA-Z]{2,})$',

  /**
   * To check the format of a strongswan id.
   * Format: Ip/Words/key
   */
  IKE_SERVER_ID = '^[^.]+[.*a-z0-9.]+[^.]$',

  /**
   * Device mac address.
   * Format: 1A:34:1T:Y4.1B:34
   */
  ADRESS_MAC = '^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$',

  /**
   * Frequency either second or minute or hour.
   * Format:
   * ```js
   * "3600/s"
   * ```
   */
  FREQUENCY = '^[0-9]{1,4}/[smh]$',

  /**
   * Code of a TCP or UDP protocol.
   * Format:
   * ```js
   * "80" | "80:100"
   * ```
   */
  TCP_UDP_PROTOCOL = '^[1-9][0-9]{0,5}$|^[1-9][0-9]{0,5}:[1-9][0-9]{0,5}$',

  /**
   * Serial number of a fortalicia case.
   * Format:
   * ```js
   * "1A34.1TY4.1B34"
   * ```
   */
  FORTALICIA_SERIAL_NUMBER = '^([a-zA-Z0-9]{4,4})[.]([a-zA-Z0-9]{4,4})[.]([a-zA-Z0-9]{4,4})$',

  /**
   * Model and revision of fortalicia boxes.
   * Format:
   * ```js
   * "FTA15-A1"
   * ```
   */
  FORTALICIA_MODEL = '^FTA(15|35|50|100)-[A-Z][1-9]$',

  /**
   * Connection URL with RabitMq.
   */
  URL_AMQP = '^amqp:\/\/(.+):(.+)(@)([a-zA-Z0-9\-.:]+)$',

  /**
   * Corresponds to the format of a CLOUD login token.
   * Format: jwt Token
   */
  TOKEN_FORMAT = '(^[A-Za-z0-9-_]*\\.[A-Za-z0-9-_]*\\.[A-Za-z0-9-_]*$)',

  /**
   * Physical / postal address including additional address.
   */
  ADRESS_LOCATION = '^[a-zA-Z0-9-/\',_. à-żÀ-Ż]{0,200}$',

  /**
   * Corresponds to the name of a city.
   */
  CITY = '^[a-zA-Z-0-9/\'. à-żÀ-Ż]{0,100}$',
}
