import { REGEX } from './regex';

describe('Regex', () => {

  describe('USER_LOGIN', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_LOGIN).test('test');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true with numbers', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_LOGIN).test('test55');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because no uppercase', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_LOGIN).test('tESt');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs at least 4 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_LOGIN).test('tes');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs at most 24 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_LOGIN).test('t'.repeat(25));
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because special chars', () => {
      // act
      const result1: boolean = new RegExp(REGEX.USER_LOGIN).test('test@');
      const result2: boolean = new RegExp(REGEX.USER_LOGIN).test('test!');
      const result3: boolean = new RegExp(REGEX.USER_LOGIN).test('test/');
      const result4: boolean = new RegExp(REGEX.USER_LOGIN).test('testé');
      // assert
      expect(result1).toStrictEqual(false);
      expect(result2).toStrictEqual(false);
      expect(result3).toStrictEqual(false);
      expect(result4).toStrictEqual(false);
    });
  });

  describe('USER_CODE', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_CODE).test('iacha');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true with number at the end', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_CODE).test('iacha11');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because uppercase', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_CODE).test('iACh');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because number inside string part', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_CODE).test('iA1Ch');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because white space', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_CODE).test('iACh 1');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs 5 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_CODE).test('iach');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs at most 5 chars for the number part', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_CODE).test('iacha111111');
      // assert
      expect(result).toStrictEqual(false);
    });
  });

  describe('USER_MAIL', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_MAIL).test('nai.tenetahc@ifex.rf');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true too', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_MAIL).test('tenetahc@ifex.rf');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because it needs a "@"', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_MAIL).test('nai.tenetahcifex.rf');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because local part needs at most 95 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_MAIL).test(`${'i'.repeat(96)}@ifex.rf`);
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because domain\s first part needs at most 95 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_MAIL).test(`nai.tenetahc@${'i'.repeat(96)}.rf`);
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because domain\s second part needs at least 2 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_MAIL).test(`nai.tenetahc@ifex.${'i'.repeat(1)}`);
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because domain\s second part needs at most 65 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_MAIL).test(`nai.tenetahc@ifex.${'i'.repeat(66)}`);
      // assert
      expect(result).toStrictEqual(false);
    });

  });

  describe('USER_NAME', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_NAME).test('ianou');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true with uppercase', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_NAME).test('iaNou');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true with white space inside', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_NAME).test('iaNo u');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true with white special chars "/\' à-ż" inside', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_NAME).test('ian/\'à-ż');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because it needs at least 2 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_NAME).test('i'.repeat(1));
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs at most 48 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.USER_NAME).test('i'.repeat(49));
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because of special chars', () => {
      // act
      const result1: boolean = new RegExp(REGEX.USER_NAME).test('ianou@');
      const result2: boolean = new RegExp(REGEX.USER_NAME).test('ianou!');
      const result3: boolean = new RegExp(REGEX.USER_NAME).test('ianou?');
      const result4: boolean = new RegExp(REGEX.USER_NAME).test('ianou<');
      const result5: boolean = new RegExp(REGEX.USER_NAME).test('ianou>');
      // assert
      expect(result1).toStrictEqual(false);
      expect(result2).toStrictEqual(false);
      expect(result3).toStrictEqual(false);
      expect(result4).toStrictEqual(false);
      expect(result5).toStrictEqual(false);
    });

  });

  describe('SPECIAL_CHAR_USER_NAME', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.SPECIAL_CHAR_USER_NAME).test('_-/\' à-żÀ-Ż');
      // assert
      expect(result).toStrictEqual(true);
    });
  });

  describe('PASSWORD', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.PASSWORD).test('I@nou1');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because it needs at least one special chars "à-żÀ-Ż!@#$%&*()-_=+<>/|?.,;^¨"', () => {
      // act
      const result: boolean = new RegExp(REGEX.PASSWORD).test('Ianou1');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because no numbers', () => {
      // act
      const result: boolean = new RegExp(REGEX.PASSWORD).test('I@@nou');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs at least 6 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.PASSWORD).test('I@no1');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs at most 50 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.PASSWORD).test(`I@nou${'1'.repeat(46)}`);
      // assert
      expect(result).toStrictEqual(false);
    });
  });

  describe('OBJECT_NAME', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.OBJECT_NAME).test('test');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true with uppercase', () => {
      // act
      const result: boolean = new RegExp(REGEX.OBJECT_NAME).test('tESt');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true with numbers', () => {
      // act
      const result: boolean = new RegExp(REGEX.OBJECT_NAME).test('test55');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true with space inside', () => {
      // act
      const result: boolean = new RegExp(REGEX.OBJECT_NAME).test('te st');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true with "çàâäéèêëîïôöùûüÿ@_.\'/"', () => {
      // act
      const result: boolean = new RegExp(REGEX.OBJECT_NAME).test('à-ż@_.\'/');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because no white space at the begining', () => {
      // act
      const result: boolean = new RegExp(REGEX.OBJECT_NAME).test(' test');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because no white space at the end', () => {
      // act
      const result: boolean = new RegExp(REGEX.OBJECT_NAME).test('test ');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs at least 3 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.OBJECT_NAME).test('te');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs at most 50 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.OBJECT_NAME).test('t'.repeat(51));
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because special char', () => {
      // act
      const result1: boolean = new RegExp(REGEX.OBJECT_NAME).test('test!');
      const result2: boolean = new RegExp(REGEX.OBJECT_NAME).test('test?');
      const result3: boolean = new RegExp(REGEX.OBJECT_NAME).test('test<');
      const result4: boolean = new RegExp(REGEX.OBJECT_NAME).test('test>');
      // assert
      expect(result1).toStrictEqual(false);
      expect(result2).toStrictEqual(false);
      expect(result3).toStrictEqual(false);
      expect(result4).toStrictEqual(false);
    });
  });

  describe('DESCRIPTION', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.DESCRIPTION).test('Incredible crazy text');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true with special char "/\'_.@ à-żÀ-Ż"', () => {
      // act
      const result: boolean = new RegExp(REGEX.DESCRIPTION).test('Incredible crazy text /\'_.@ à-żÀ-Ż');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because it needs at most 100 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.DESCRIPTION).test('I'.repeat(101));
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because special chars', () => {
      // act
      const result1: boolean = new RegExp(REGEX.DESCRIPTION).test('Text <');
      const result2: boolean = new RegExp(REGEX.DESCRIPTION).test('Text >');
      const result3: boolean = new RegExp(REGEX.DESCRIPTION).test('Text $');
      // assert
      expect(result1).toStrictEqual(false);
      expect(result2).toStrictEqual(false);
      expect(result3).toStrictEqual(false);
    });
  });

  describe('IKE_CONNECTION_NAME', () => {
    it('Should return true for a 1 "part"', () => {
      // act
      const result: boolean = new RegExp(REGEX.IKE_CONNECTION_NAME).test('ian');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true for 2 "part"', () => {
      // act
      const result: boolean = new RegExp(REGEX.IKE_CONNECTION_NAME).test('ian-ou');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true with number inside', () => {
      // act
      const result: boolean = new RegExp(REGEX.IKE_CONNECTION_NAME).test('1an-0u');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because it needs the first "part"', () => {
      // act
      const result: boolean = new RegExp(REGEX.IKE_CONNECTION_NAME).test('-ou');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs the second "part" if the dash is present', () => {
      // act
      const result: boolean = new RegExp(REGEX.IKE_CONNECTION_NAME).test('ian-');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs at least 3 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.IKE_CONNECTION_NAME).test('ia');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs at most 25 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.IKE_CONNECTION_NAME).test('i'.repeat(26));
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because of special char', () => {
      // act
      const result: boolean = new RegExp(REGEX.IKE_CONNECTION_NAME).test('i@n');
      // assert
      expect(result).toStrictEqual(false);
    });
  });

  describe('DNS_NAME', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.DNS_NAME).test('ianchat');
      // assert
      expect(result).toStrictEqual(true);
    });
  });

  describe('IKE_SERVER_ID', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.IKE_SERVER_ID).test('ian');
      // assert
      expect(result).toStrictEqual(true);
    });
  });

  describe('ADRESS_MAC', () => {
    it('Should return true with "-"', () => {
      // act
      const result: boolean = new RegExp(REGEX.ADRESS_MAC).test('aa-bb-11-bb-ff-22');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true with ":"', () => {
      // act
      const result: boolean = new RegExp(REGEX.ADRESS_MAC).test('aa:bb:11:bb:ff:22');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because more than 6 "block"', () => {
      // act
      const result: boolean = new RegExp(REGEX.ADRESS_MAC).test('aa:bb:11:bb:ff:22:zz');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because less than 6 "block"', () => {
      // act
      const result: boolean = new RegExp(REGEX.ADRESS_MAC).test('aa:bb:11:bb:ff');
      // assert
      expect(result).toStrictEqual(false);
    });

  });

  describe('FREQUENCY', () => {
    it('Should return true for seconds', () => {
      // act
      const result: boolean = new RegExp(REGEX.FREQUENCY).test('11/s');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true for minutes', () => {
      // act
      const result: boolean = new RegExp(REGEX.FREQUENCY).test('11/m');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true for hours', () => {
      // act
      const result: boolean = new RegExp(REGEX.FREQUENCY).test('11/h');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false it needs at least 1 number', () => {
      // act
      const result: boolean = new RegExp(REGEX.FREQUENCY).test('/h');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false it needs at most 4 number', () => {
      // act
      const result: boolean = new RegExp(REGEX.FREQUENCY).test('11111/h');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false it needs the "/"', () => {
      // act
      const result: boolean = new RegExp(REGEX.FREQUENCY).test('1s');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false it needs the time unit', () => {
      // act
      const result: boolean = new RegExp(REGEX.FREQUENCY).test('1/');
      // assert
      expect(result).toStrictEqual(false);
    });
  });

  describe('TCP_UDP_PROTOCOL', () => {
    it('Should return true for a 2 part', () => {
      // act
      const result: boolean = new RegExp(REGEX.TCP_UDP_PROTOCOL).test('1');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true for a 2 part', () => {
      // act
      const result: boolean = new RegExp(REGEX.TCP_UDP_PROTOCOL).test('1:1');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false for a 1 part because it needs at most 6 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.TCP_UDP_PROTOCOL).test('1'.repeat(7));
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false for a 2 part because it needs at most 6 chars for the first part', () => {
      // act
      const result: boolean = new RegExp(REGEX.TCP_UDP_PROTOCOL).test(`${'1'.repeat(7)}:1`);
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false for a 2 part because it needs at most 6 chars for the second part', () => {
      // act
      const result: boolean = new RegExp(REGEX.TCP_UDP_PROTOCOL).test(`1:${'1'.repeat(7)}`);
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false for a 2 part because it accepts only numbers', () => {
      // act
      const result: boolean = new RegExp(REGEX.TCP_UDP_PROTOCOL).test('1:a');
      // assert
      expect(result).toStrictEqual(false);
    });

  });

  describe('FORTALICIA_SERIAL_NUMBER', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.FORTALICIA_SERIAL_NUMBER).test('a11b.c22d.e33f');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because it needs 3 parts', () => {
      // act
      const result1: boolean = new RegExp(REGEX.FORTALICIA_SERIAL_NUMBER).test('a11b');
      const result2: boolean = new RegExp(REGEX.FORTALICIA_SERIAL_NUMBER).test('a11b.c22d');
      const result3: boolean = new RegExp(REGEX.FORTALICIA_SERIAL_NUMBER).test('a11b.c22d.e33f.g44h');
      // assert
      expect(result1).toStrictEqual(false);
      expect(result2).toStrictEqual(false);
      expect(result3).toStrictEqual(false);
    });

    it('Should return false because it accepts only numbers of alphabetical char', () => {
      // act
      const result: boolean = new RegExp(REGEX.FORTALICIA_SERIAL_NUMBER).test('@.c22d.e33f');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because each part must have 4 chars', () => {
      // act
      const result1: boolean = new RegExp(REGEX.FORTALICIA_SERIAL_NUMBER).test('a.c22d.e33f');
      const result2: boolean = new RegExp(REGEX.FORTALICIA_SERIAL_NUMBER).test('a11b.c2.e33f');
      const result3: boolean = new RegExp(REGEX.FORTALICIA_SERIAL_NUMBER).test('a11b.c22d.e33');
      // assert
      expect(result1).toStrictEqual(false);
      expect(result2).toStrictEqual(false);
      expect(result3).toStrictEqual(false);
    });

  });

  describe('FORTALICIA_MODEL', () => {
    it('Should return true for 15', () => {
      // act
      const result: boolean = new RegExp(REGEX.FORTALICIA_MODEL).test('FTA15-A1');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true for 35', () => {
      // act
      const result: boolean = new RegExp(REGEX.FORTALICIA_MODEL).test('FTA35-A1');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true for 50', () => {
      // act
      const result: boolean = new RegExp(REGEX.FORTALICIA_MODEL).test('FTA50-A1');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return true for 100', () => {
      // act
      const result: boolean = new RegExp(REGEX.FORTALICIA_MODEL).test('FTA100-A1');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because it needs FTA(15|35|50|100)', () => {
      // act
      const result: boolean = new RegExp(REGEX.FORTALICIA_MODEL).test('test-A1');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs the "-"', () => {
      // act
      const result: boolean = new RegExp(REGEX.FORTALICIA_MODEL).test('test');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs 1 alphabetical char and 1 number after the "-"', () => {
      // act
      const result: boolean = new RegExp(REGEX.FORTALICIA_MODEL).test('test-');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because the part after the "-" must be 1 alphabetical and 1 number in this order', () => {
      // act
      const result: boolean = new RegExp(REGEX.FORTALICIA_MODEL).test('test-1b');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because alpha char after the "-" must be uppercase', () => {
      // act
      const result: boolean = new RegExp(REGEX.FORTALICIA_MODEL).test('test-z1');
      // assert
      expect(result).toStrictEqual(false);
    });
  });

  describe('URL_AMQP', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.URL_AMQP).test('amqp://chatenet:ian@localhost');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because it needs the ":" part', () => {
      // act
      const result: boolean = new RegExp(REGEX.URL_AMQP).test('amqp://chatenetian@localhost');
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because it needs the "@" part', () => {
      // act
      const result: boolean = new RegExp(REGEX.URL_AMQP).test('amqp://chatenet:ianlocalhost');
      // assert
      expect(result).toStrictEqual(false);
    });
  });

  describe('TOKEN_FORMAT', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.TOKEN_FORMAT).test('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false', () => {
      // act
      const result: boolean = new RegExp(REGEX.TOKEN_FORMAT).test('slt');
      // assert
      expect(result).toStrictEqual(false);
    });
  });

  describe('ADRESS_LOCATION', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.ADRESS_LOCATION).test('croisement D8 et, D992, 23340');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because it needs at most 200 char', () => {
      // act
      const result: boolean = new RegExp(REGEX.ADRESS_LOCATION).test('1'.repeat(201));
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because special char', () => {
      // act
      const result: boolean = new RegExp(REGEX.ADRESS_LOCATION).test('!croisement D8 et, D992, 23340');
      // assert
      expect(result).toStrictEqual(false);
    });
  });

  describe('CITY', () => {
    it('Should return true', () => {
      // act
      const result: boolean = new RegExp(REGEX.CITY).test('Gentioux-Pigerolles');
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because it needs at most 100 chars', () => {
      // act
      const result: boolean = new RegExp(REGEX.CITY).test('G'.repeat(101));
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return true with number', () => {
      // act
      const result: boolean = new RegExp(REGEX.CITY).test('Genti0ux-Pigerolles');
      // assert
      expect(result).toStrictEqual(true);
    });
  });
});
