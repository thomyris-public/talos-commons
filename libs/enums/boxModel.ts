export enum BOX_MODEL {
    FTA15A1 = 'FTA15-A1',
    FTA15A2 = 'FTA15-A2',
    FTA15B1 = 'FTA15-B1',
}