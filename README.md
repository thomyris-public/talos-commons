# Talos Commons

## Summary

* [Address](#address)
* [IpRange](#iprange)
* [Enums](#enums)

## Address

### Examples

```js
const address = new Address('10.0.0.100/26');

{
  // The address as string
  address: '10.0.0.100/26',
  // The ip address in IP/MASK
  ip: '10.0.0.100',
  // The mask for the address
  mask: 26,
  // The ip to bigInteger
  bigInteger: BigInteger { '0': 167772260, '1': 0, t: 1, s: 0 },
  // Network of the address like <ip>/<mask>
  network: '10.0.0.64/26',
  // If the address is equal to the network address
  isNetwork: false,
  // If the address has mask 32
  isHost: false,
  // The network size without reserved address
  networkSize: BigInteger { '0': 61, s: 0, t: 1 },
  // The subnetMask for the address (full minus size)
  subnetMask: '255.255.255.192',
  // The wildcardMask for the address (empty plus size)
  wildcardMask: '0.0.0.63',
  // The broadcast for the address
  broadcast: '10.0.0.127',
  // If the address is equal to the broadcast ip
  isBroadcast: false,
  // The fist host of network
  firstHost: '10.0.0.65',
  // The last host of network
  lastHost: '10.0.0.126',
  // If the address is all 0.0.0.0/0
  isAll: false
}

const address = new Address('10.0.0.42');
const address = new Address(new Address4('10.0.0.1/24'));
const address = new Address('10.0.0.64', 26);

expect(address.toString()).toStrictEqual('10.0.0.64/26');

// Compare to an other address, if network use first address
// return + if this > a, - if this < a, 0 if equal
const result = address1.compareTo(address2);

// check to an other address
// return -1 if other have this
// return 0 if this don't have other
// return 1 if this have other
const result = address1.contain(address2);

// If the address is private
// Not in private A 10.0.0.0/8
// Not in private B 172.16.0.0/12
// Not in private C 192.168.0.0/16
const result = address.isPrivate();

// If the address is reserved
// Not in loopback 127.0.0.0/8
// Not in APIPA 169.254.0.0/16
// Not in multicast 224.0.0.0/4
// Not in CGN RFC6598 100.64.0.0/10 
const result = address.isReserved();

// If is in multicast 224.0.0.0/4
const result = address.isMulticast();

// Return the family of the address.
// returns: 'Network' or 'Host'
const result = address.getFamily();

// Check if the host address is in the range of the other host addresses.
// Return {true} if the address is in the range.
// Return {false} if the address is above or below the range.
const start = new Address('10.1.0.10');
const end = new Address('10.1.0.25')
const result = address.isInRange(start, end);
```
## IpRange

### Examples

```js
const range = new IpRange({start: '10.0.0.1', end: '10.0.0.20'});
const range2  = new IpRange({start: '10.0.0.5', end: '10.0.0.10'});

// Splits a range by an array of ranges
// Returns an array of ranges
const newRangeArray = range.cutRange([range2]);

// Check if the second range is OUTSIDE of the firstRange or is OVERSIZED
IpRange.checkRange(range, range2);

// Check if the secondRange is inside the firstRange
IpRange.isInRange(range, range2);

// Return true if the secondRange is overlaping the firstRange
IpRange.isOverlaping(range, range2);
```

## Enums

* ERROR
* REGEX
* USER_PROFILE
* USER_ROLE
* BOX_STATUS
