import { join } from 'path';
import { promises } from 'fs';

import { REGEX } from './libs/enums/regex';

module.exports = async () => {
  const path = join('libs', 'enums', 'regex.spec.ts');
  const file = await promises.readFile(path, 'utf-8');

  const enumValues: string[] = Object.keys(REGEX);
  const notTested: string[] = [];

  for (let i = 0; i < enumValues.length; i++) {
    const enumValue = enumValues[i];
    const isInFile: boolean = file.includes(enumValue);
    if (isInFile === false) notTested.push(enumValue);
  }

  if (notTested.length > 0) {
    let error = '\n\x1b[31mTest all Regex please !\x1b[0m\nMissing regex:\n';

    for (let i = 0; i < notTested.length; i++) {
      const regex = notTested[i];
      error += `- ${regex}\n`;
    }
    console.error(error);
    throw new Error(error);
  }
};
